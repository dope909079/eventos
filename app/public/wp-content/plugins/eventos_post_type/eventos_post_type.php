<?php
    /*
        Plugin Name: eventos - Post Types
        Version: 1.0.0
        Text Domain: eventos
    */

    function eventos_post_type() {
 
        $labels = array(
            'name'                  => _x( 'Eventos', 'Post Type General Name', 'eventos' ),
            'singular_name'         => _x( 'Evento', 'Post Type Singular Name', 'eventos' ),
            'menu_name'             => __( 'Eventos', 'eventos' ),
            'name_admin_bar'        => __( 'Evento', 'eventos' ),
            'archives'              => __( 'Archivo', 'eventos' ),
            'attributes'            => __( 'Atributos', 'eventos' ),
            'parent_item_colon'     => __( 'Clase padre', 'eventos' ),
            'all_items'             => __( 'Todos los eventos', 'eventos' ),
            'add_new_item'          => __( 'Agregar evento', 'eventos' ),
            'add_new'               => __( 'Agregar evento', 'eventos' ),
            'new_item'              => __( 'Nuevo evento', 'eventos' ),
            'edit_item'             => __( 'Editar evento', 'eventos' ),
            'update_item'           => __( 'Actualizar evento', 'eventos' ),
            'view_item'             => __( 'Ver evento', 'eventos' ),
            'view_items'            => __( 'Ver eventos', 'eventos' ),
            'search_items'          => __( 'Buscar evento', 'eventos' ),
            'not_found'             => __( 'No encontrado', 'eventos' ),
            'not_found_in_trash'    => __( 'No encontrado en papelera', 'eventos' ),
            'featured_image'        => __( 'Imagen destacada', 'eventos' ),
            'set_featured_image'    => __( 'Guardar imagen destacada', 'eventos' ),
            'remove_featured_image' => __( 'Eliminar imagen destacada', 'eventos' ),
            'use_featured_image'    => __( 'Utilizar como imagen destacada', 'eventos' ),
            'insert_into_item'      => __( 'Insertar en evento', 'eventos' ),
            'uploaded_to_this_item' => __( 'Agregado en evento', 'eventos' ),
            'items_list'            => __( 'Lista de eventos', 'eventos' ),
            'items_list_navigation' => __( 'Navegación de eventos', 'eventos' ),
            'filter_items_list'     => __( 'Filtrar eventos', 'eventos' ),
        );
        $args = array(
            'label'                 => __( 'Evento', 'eventos' ),
            'description'           => __( 'Eventos para el sitio web', 'eventos' ),
            'labels'                => $labels,
            'supports'              => array( 'title', 'editor', 'thumbnail' ),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 6,
            'menu_icon'             => 'dashicons-tag',
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => true,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'capability_type'       => 'page',
        );
        register_post_type( 'eventos', $args );
     
    }
    add_action( 'init', 'eventos_post_type', 0 );
?>